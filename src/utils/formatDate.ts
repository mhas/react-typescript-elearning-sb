export default function (d: Date): string {
  const date = new Date(d);

  const formattedDate = Intl.DateTimeFormat(undefined, {
    year: "numeric",
    month: "numeric",
    day: "numeric",
  }).format(date);

  return formattedDate;
}

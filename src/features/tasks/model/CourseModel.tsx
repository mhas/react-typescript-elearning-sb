import ChapterModel from "./ChapterModel";

interface CourseModel {
  courseId: string;
  courseName: string;
  chapters: Array<ChapterModel>;
}

export default CourseModel;

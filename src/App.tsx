import TaskContainer from "./features/tasks/TaskContainer";

function App() {
  return <TaskContainer />;
}

export default App;

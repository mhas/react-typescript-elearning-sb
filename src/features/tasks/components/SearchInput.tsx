import { SyntheticEvent } from "react";
import { Form, InputGroup } from "react-bootstrap";

type SearchInputProps = {
  value?: string;
  className?: string;
  onChange: (value: string) => any;
};

const SearchInput = ({ className, onChange, value }: SearchInputProps) => {
  const handleOnChange = (event: SyntheticEvent) => {
    const target = event.target as HTMLInputElement;

    onChange(target.value);
  };
  return (
    <>
      <Form.Group className="mb-3">
        <Form.Label className="d-inline-block fs-6 mb-1 ms-2 form-label">
          Search
        </Form.Label>
        <InputGroup className={`mb-3 ${className}`} style={{ height: "30px" }}>
          <Form.Control
            aria-label="Search text"
            className="bg-light border-0 fs-7 fw-bold"
            onChange={handleOnChange}
            value={value}
          />
          <InputGroup.Text className="bg-light border-0">
            <div className="border-start d-inline-block">
              <img className="ps-2 d-block" src="./search-icon.svg" alt="" />
            </div>
          </InputGroup.Text>
        </InputGroup>
      </Form.Group>
    </>
  );
};

export default SearchInput;

const ENDPOINT = "/tasks";

export function getTasks() {
  return fetch(ENDPOINT)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
    });
}

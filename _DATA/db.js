const tasks = require("./tasks.json");
const courses = require("./courses.json");

module.exports = () => ({
  courses,
  tasks,
});

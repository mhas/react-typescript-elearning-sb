import { Accordion, Col, Container, Row } from "react-bootstrap";
import { useCourses } from "./context/TasksContext";
import TaskModel from "./model/TaskModel";
import TaskItem from "./components/TaskItem";
import FilterSelect, { Option } from "./components/FilterSelect";
import SearchInput from "./components/SearchInput";
import { mapChapters, mapCourses } from "../../utils/selectMapper";

const TaskView = () => {
  const {
    course,
    courses,
    setCourse,
    setChapter,
    chapter,
    chapterTasks,
    courseChapters,
    setSearchText,
  } = useCourses();

  function handeChangeCourse(value: Option | null) {
    setCourse(courses.find((course) => course.courseId === value?.id));
  }

  function handeSelectChapter(value: Option | null) {
    setChapter(
      course.chapters.find((chapter) => chapter.chapterId === value?.id)
    );
  }

  function handeOnChangeSearchInput(value: string) {
    setSearchText(value);
  }

  return (
    <>
      <Container className="mt-5">
        <Row>
          <Col md={{ span: 3, offset: 1 }}>
            <FilterSelect
              className="mb-3"
              onChange={handeChangeCourse}
              icon="./course-icon.svg"
              label="Course"
              value={mapCourses([course])[0]}
              options={mapCourses(courses)}
            />
            <FilterSelect
              className="mb-3"
              onChange={handeSelectChapter}
              icon="./chapter-icon.svg"
              label="Chapter"
              value={mapChapters([chapter])[0]}
              options={mapChapters(courseChapters)}
            />
            <SearchInput onChange={handeOnChangeSearchInput} />
          </Col>

          <Col md={6}>
            {chapterTasks && (
              <Accordion defaultActiveKey="0">
                {chapterTasks.map((task: TaskModel) => (
                  <TaskItem task={task} key={task.taskId} />
                ))}
              </Accordion>
            )}
            {!chapterTasks.length && (
              <div className="alert alert-primary" role="alert">
                Nie znaleziono zadań
              </div>
            )}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default TaskView;

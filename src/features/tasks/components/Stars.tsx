type StarsProps = {
  level: number;
};

const Stars = ({ level }: StarsProps) => {
  const MAX_LEVEL = 5;

  const stars = [];

  for (let index = 1; index <= MAX_LEVEL; index++) {
    if (index <= level) {
      stars.push(
        <img
          className="ms-1"
          style={{ width: "11px" }}
          key={index}
          src="./star-filled-icon.svg"
          alt=""
        />
      );
      continue;
    }
    stars.push(
      <img
        className="ms-1"
        style={{ width: "11px" }}
        key={index}
        src="./star-icon.svg"
        alt=""
      />
    );
  }

  return <div>{stars}</div>;
};

export default Stars;

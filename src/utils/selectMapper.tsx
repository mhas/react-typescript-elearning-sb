import ChapterModel from "../features/tasks/model/ChapterModel";
import CourseModel from "../features/tasks/model/CourseModel";

export const mapCourses = (courses: CourseModel[]) =>
  courses.map((course) => ({
    id: course.courseId,
    value: course.courseName,
  }));

export const mapChapters = (chapters: ChapterModel[]) =>
  chapters.map((chapter) => ({
    id: chapter.chapterId,
    value: chapter.chapterName,
  }));

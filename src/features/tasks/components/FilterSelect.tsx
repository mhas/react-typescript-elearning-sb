import { ButtonGroup, Dropdown } from "react-bootstrap";

export type Option = {
  id: string;
  value: string;
};

type FilterSelectProps = {
  icon: string;
  options: Option[];
  value?: Option | null;
  label: string;
  className?: string;
  onChange: (selectedOption: Option | null) => void;
};

const FilterSelect = ({
  icon,
  value,
  options,
  label,
  onChange,
  className,
}: FilterSelectProps) => {
  const handleOnChange = (eventKey: string | null) => {
    const selectedValue = eventKey;

    const selectedOption =
      options.find((option) => option.id === selectedValue) || null;
    onChange(selectedOption);
  };

  return (
    <>
      <small className="d-inline-block fs-6 mb-1 ms-2 form-label">
        {label}
      </small>
      <Dropdown
        as={ButtonGroup}
        variant="link"
        className={`w-100 ${className}`}
        onSelect={handleOnChange}
        style={{ height: "40px" }}
      >
        <Dropdown.Toggle
          disabled={!value?.id}
          bsPrefix="p-0"
          childBsPrefix="p-0"
          split
          className="btn bg-light px-4 py-2 d-flex align-items-center rounded-2 w-100 border-0"
        >
          <div className="border-end pe-4">
            <img style={{ width: "25px" }} src={icon} alt="" />
          </div>
          <span className="mx-2 fs-7 fw-bold">{value?.value || "-"}</span>
          <img className="ms-auto" src="./caret-down-icon.svg" alt="" />
        </Dropdown.Toggle>

        <Dropdown.Menu className="w-100 shadow-sm border-0">
          {options.map((option: Option) => (
            <Dropdown.Item key={option.id} eventKey={option.id}>
              {option.value}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default FilterSelect;

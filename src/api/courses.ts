const ENDPOINT = "/courses";

export function getCourses() {
  return fetch(ENDPOINT)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
    });
}

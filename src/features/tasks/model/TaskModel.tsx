interface TaskModel {
  taskId?: string;
  parentChapterId: string;
  parentCourseId: string;
  title: string;
  shortDescription: string;
  longDescription: string;
  difficultLevel: string;
  initialImplementationCode: string;
  initialUnitTestsCode: string;
  unitTestsToBeExecutedCode: string;
  author: string;
  completedBy: string;
  rewardToGain: string;
  progressStatus: string;
  startDate: Date;
  dueDate: Date;
  totalTryAmount: string;
  inputOutputList: [];
}

export default TaskModel;

import { useEffect, useState, createContext, useContext } from "react";
import { getCourses } from "../../../api/courses";
import { getTasks } from "../../../api/tasks";
import CourseModel from "../model/CourseModel";
import TaskModel from "../model/TaskModel";
import ChapterModel from "../model/ChapterModel";
interface ContextProps {
  courses: CourseModel[];
  loading: boolean;
  course: CourseModel;
  setCourse: any;
  setChapter: any;
  chapterTasks: TaskModel[];
  courseChapters: ChapterModel[];
  chapter: ChapterModel;
  setSearchText: any;
}

const TasksContext = createContext<ContextProps>({} as ContextProps);

export const useCourses = () => useContext(TasksContext);

const TasksContextProvider = (props: any): any => {
  const [loading, setLoading] = useState(true);
  const [courses, setCourses] = useState<CourseModel[]>([]);
  const [course, setCourse] = useState<CourseModel>({} as CourseModel);
  const [tasks, setTasks] = useState<TaskModel[]>([]);
  const [chapter, setChapter] = useState<ChapterModel>({} as ChapterModel);
  const [courseChapters, setCourseChapters] = useState<ChapterModel[]>([]);
  const [chapterTasks, setTasksByChapter] = useState<TaskModel[]>([]);
  const [searchText, setSearchText] = useState<string>("");

  useEffect(() => {
    getCourses().then((courses: CourseModel[]) => {
      setLoading(false);
      setCourses(courses);

      if (courses.length) {
        setCourse(courses[0]);
      }
    });

    getTasks().then((tasks: TaskModel[]) => {
      setLoading(false);
      setTasks(tasks);
    });
  }, []);

  useEffect(() => {
    const tasks = getTasksByChapterId(chapter.chapterId);

    if (searchText.length > 0) {
      setTasksByChapter(
        tasks.filter((task: TaskModel) =>
          task.title
            .toLocaleLowerCase()
            .includes(searchText.toLocaleLowerCase())
        )
      );
    } else {
      setTasksByChapter(tasks);
    }
  }, [searchText]);

  useEffect(() => {
    setCourseChapters(course.chapters || []);

    if (course?.chapters?.length) {
      setChapter(course.chapters[0]);
    } else {
      setChapter({} as ChapterModel);
    }
  }, [course]);

  useEffect(() => {
    setTasksByChapter(getTasksByChapterId(chapter.chapterId));
  }, [chapter]);

  const getTasksByChapterId = (id: string) => {
    return tasks.filter(
      (task: TaskModel) => parseInt(task.parentChapterId) === parseInt(id)
    );
  };

  const value = {
    loading,
    course,
    courses,
    setCourse,
    setChapter,
    chapterTasks,
    courseChapters,
    chapter,
    setSearchText,
  };

  return (
    <TasksContext.Provider value={value}>
      {props.children}
    </TasksContext.Provider>
  );
};

export default TasksContextProvider;

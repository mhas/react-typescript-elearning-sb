import { Accordion } from "react-bootstrap";
import TaskModel from "../model/TaskModel";
import formatDate from "../../../utils/formatDate";
import Stars from "./Stars";

type TaskItemProps = {
  task: TaskModel;
};

const TaskItem = ({ task }: TaskItemProps) => {
  return (
    <Accordion.Item eventKey={task.taskId} className="mb-3 border">
      <Accordion.Header>
        <div className="d-flex w-100">
          <div className="border-end pe-3 me-3">
            <img className="h-100 w-100" src="./task-icon.svg" alt="" />
          </div>
          <div className="w-100 pe-3">
            <div className="d-flex justify-content-between mb-2">
              <h2 className="h5 mb-0">{task.title}</h2>
              <Stars level={parseInt(task.difficultLevel)} />
            </div>
            <hr className="my-1" />
            <ul className="mb-0 ps-0 w-100 d-flex justify-content-between align-items-center">
              <li className="mt-1 d-inline fs-7">
                <img
                  style={{ width: "11px" }}
                  className="align-baseline me-1"
                  src="./trophy-icon.svg"
                  alt=""
                />
                {task.rewardToGain}
              </li>
              <li className="mt-1 d-inline fs-7">
                <img
                  style={{ width: "11px" }}
                  className="align-baseline me-1"
                  src="./correct-icon.svg"
                  alt=""
                />
                {task.completedBy} of {task.totalTryAmount}
              </li>
              <li className="mt-1 d-inline fs-7">
                <img
                  style={{ width: "11px" }}
                  className="align-baseline me-1"
                  src="./person-icon.svg"
                  alt=""
                />
                {task.author}
              </li>
              <li className="mt-1 d-inline fs-7">
                <img
                  style={{ width: "11px" }}
                  className="align-baseline me-1"
                  src="./time-icon.svg"
                  alt=""
                />
                {formatDate(task.dueDate)}
              </li>
              <li className="d-inline">
                <span className="fs-8 px-4 py-1 border text-uppercase d-inline-block">
                  Algorythms
                </span>
              </li>
            </ul>
          </div>
        </div>
      </Accordion.Header>
      <Accordion.Body></Accordion.Body>
    </Accordion.Item>
  );
};

export default TaskItem;

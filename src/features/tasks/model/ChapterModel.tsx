import TaskModel from "./TaskModel";

interface ChapterModel {
  chapterId: string;
  chapterName: string;
  tasks: Array<TaskModel>;
}

export default ChapterModel;

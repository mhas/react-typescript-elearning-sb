const { fetch: baseFetch } = window;

const API_URL = import.meta.env.VITE_API_URL;

window.fetch = async (...args) => {
  const [resource, config] = args;

  const response = await baseFetch(`${API_URL}${resource}`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    ...config,
  });

  return response;
};

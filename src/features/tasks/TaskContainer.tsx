import TasksContextProvider from "./context/TasksContext";
import TaskView from "./TaskView";

const TaskContainer = () => {
  return (
    <TasksContextProvider>
      <TaskView />
    </TasksContextProvider>
  );
};

export default TaskContainer;
